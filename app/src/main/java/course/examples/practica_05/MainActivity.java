package course.examples.practica_05;


import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.view.View;
import android.view.View.OnClickListener;

public class MainActivity extends Activity {
    Button button1,button2, button3, button4 ;
    ImageView image1, image2, image3, image4;
    boolean ban;

    @Override public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        addListenerOnButton();
    }



    public void addListenerOnButton() {
        image1 = (ImageView) findViewById(R.id.imageView1);
        button1 = (Button) findViewById(R.id.btnChangeImage);

        image2 = (ImageView) findViewById(R.id.imageView2);
        button2 = (Button) findViewById(R.id.btnChangeImage2);

        image3 = (ImageView) findViewById(R.id.imageView3);
        button3 = (Button) findViewById(R.id.btnChangeImage3);

        image4 = (ImageView) findViewById(R.id.imageView4);
        button4 = (Button) findViewById(R.id.btnChangeImage4);



        button1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {

                image1.setImageResource(R.drawable.manzana); //nombre del archivo imagen

            }
        });

        button2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {

                image1.setImageResource(R.drawable.uva); //nombre del archivo imagen

            }
        });

        button3.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {

                image1.setImageResource(R.drawable.sandia); //nombre del archivo imagen

            }
        });

        button4.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {

                image1.setImageResource(R.drawable.pina); //nombre del archivo imagen

            }
        });


    }
}